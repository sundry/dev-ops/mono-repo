import { Sequelize } from 'sequelize';
import config from '../config/config';
import { UsersFactory } from './Users';
import { AccountsFactory } from './Accounts';

const env = process.env.NODE_ENV;
const configEnv = config[env];

let sequelize;
sequelize = new Sequelize(
  configEnv.database,
  configEnv.username,
  configEnv.password,
  configEnv
);

const models = {
  Users: UsersFactory(sequelize),
  Accounts: AccountsFactory(sequelize),
};

Object.values(models)
  .filter((model) => typeof model.associate === 'function')
  .forEach((model) => model.associate(models));

const db = {
  ...models,
  sequelize,
  Sequelize,
};

(async () => {
  try {
    await sequelize.authenticate();
    console.log('Database connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
})();

export default db;
